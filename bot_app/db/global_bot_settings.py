from bot_app.db.base import create_dict_con


async def get_all() -> dict:
    """
    Получение глобальных настроек бота
    :param param:
    :return: словарь где ключи это названия параметров, а значения их значения пример:
    {'greetings_message_photo': None,
     'greetings_message_text': 'привет пройдите капчу',
      'message_after_captcha': 'вы прошли капчу в чат такой-то спасибо',
       'target_channel_id': '-1001783944910',
        'target_channel_url': 'https://t.me/+4Xp5a7ZpwHg1NjZi'}

    """
    con, cur = await create_dict_con()
    await cur.execute('select * from global_bot_settings ')
    settings = await cur.fetchall()
    settings_dict = dict()
    for data in settings:
        settings_dict[data['param']] = data['value']
    await con.ensure_closed()
    return settings_dict


async def get(param) -> dict:
    """
    Получение определенного параметра из глобальных настроек
    :param param: название параметра
    :return:
    """
    con, cur = await create_dict_con()
    await cur.execute('select * from global_bot_settings where param = %s ', (param,))
    setting_param = await cur.fetchone()
    await con.ensure_closed()
    return setting_param

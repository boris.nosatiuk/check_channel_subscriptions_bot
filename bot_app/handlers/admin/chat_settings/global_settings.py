import aiogram.types
from aiogram.types import CallbackQuery, Message
from aiogram.utils.exceptions import ChatNotFound

from bot_app import config
from bot_app.db.chat_settings import get_all_chat_id, get_chat_settings, switch_bot_enabled, switch_skip_verified_users, \
    switch_kick_from_chat_on_fail, switch_ban_from_chat_on_fail
from bot_app.keyboard.inline_markup import settings_button
from bot_app.keyboard.inline_markup import all_chat_id
from bot_app.misc import dp, bot, redis
from bot_app.keyboard.reply_echo_admin_button import admin_echo_button


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text='back-to-channel-list')
async def settings_menu(call: CallbackQuery):
    await call.answer()
    all_chat_dict = await get_all_chat_id()
    chat_data_list = []
    for chat_id in all_chat_dict:
        try:
            chat_data = await bot.get_chat(chat_id['chat_id'])
            chat_data_list.append(chat_data)
        except ChatNotFound:
            Exception
    await call.message.edit_text(text='Выберите чат для настройки:', reply_markup=all_chat_id(chat_data_list))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='cancellation')
async def settings_menu(call: CallbackQuery):
    await call.answer()
    chat_id = call.data.split('_')[1]
    chat_settings = await get_chat_settings(int(chat_id))
    await call.message.edit_text(text='Меню настроек чата:', reply_markup=settings_button(chat_id, chat_settings))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='skip-Verified-Users')
async def settings_menu(call: CallbackQuery):

    await call.answer()
    chat_id = int(call.data.split('_')[1])
    await switch_skip_verified_users(chat_id)
    chat_settings = await get_chat_settings(chat_id)
    await call.message.edit_reply_markup(reply_markup=settings_button(chat_id, chat_settings))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='bot-enabled')
async def settings_menu(call: CallbackQuery):
    await call.answer()
    chat_id = int(call.data.split('_')[1])
    await switch_bot_enabled(chat_id)
    chat_settings = await get_chat_settings(chat_id)
    await call.message.edit_reply_markup(reply_markup=settings_button(chat_id, chat_settings))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='kick-from-chat-on-fail')
async def settings_menu(call: CallbackQuery):

    await call.answer()
    chat_id = int(call.data.split('_')[1])
    await switch_kick_from_chat_on_fail(chat_id)
    chat_settings = await get_chat_settings(chat_id)
    await call.message.edit_reply_markup(reply_markup=settings_button(chat_id, chat_settings))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='ban-from-chat-on-fail')
async def settings_menu(call: CallbackQuery):

    await call.answer()
    chat_id = int(call.data.split('_')[1])
    await switch_ban_from_chat_on_fail(chat_id)
    chat_settings = await get_chat_settings(chat_id)
    await call.message.edit_reply_markup(reply_markup=settings_button(chat_id, chat_settings))



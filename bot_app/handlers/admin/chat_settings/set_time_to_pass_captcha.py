import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import CallbackQuery, Message

from bot_app import config
from bot_app.db.chat_settings import update_time_to_pass_captcha, get_chat_settings
from bot_app.keyboard.inline_markup import settings_button
from bot_app.keyboard.reply_cancel_button import cancel_btn
from bot_app.misc import dp, bot


class SetTime(StatesGroup):
    new_time = State()


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='time-to-pass-captcha')
async def settings_menu(call: CallbackQuery, state: FSMContext):
    await call.answer()
    channel_id = call.data.split('_')[1]
    await call.message.edit_reply_markup(None)
    await call.message.delete()
    await bot.send_message(chat_id=call.message.chat.id, text='Введите новое время для прохождения капчи:',
                           reply_markup=cancel_btn())
    await state.set_state(SetTime.new_time)
    await state.update_data({'chat_id': channel_id})


@dp.message_handler(text='Отмена', state=SetTime.new_time)
async def cancel_new_greetings_text(message: Message, state: FSMContext):
    data = await state.get_data()
    await state.finish()
    chat_settings = await get_chat_settings(data['chat_id'])
    await bot.send_message(chat_id=message.chat.id,
                           text='Время не было изменено',
                           reply_markup=settings_button(data['chat_id'], chat_settings))


@dp.message_handler(state=SetTime.new_time)
async def get_new_greetings_text(message: Message, state: FSMContext):
    new_time = message.text
    data = await state.get_data()
    await state.finish()
    await update_time_to_pass_captcha(time=new_time, chat_id=data['chat_id'])
    chat_settings = await get_chat_settings(data['chat_id'])
    await bot.send_message(chat_id=message.chat.id,
                           text=f'Вы успешно изменили время для прохождения капчи',
                           reply_markup=settings_button(data['chat_id'], chat_settings))

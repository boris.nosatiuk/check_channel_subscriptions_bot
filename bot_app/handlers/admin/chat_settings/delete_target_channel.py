import aiogram.types
from aiogram.types import CallbackQuery

import bot_app.db.chat_settings
from bot_app import config
from bot_app.db.chat_settings import get_chat_settings
from bot_app.keyboard.inline_markup import settings_button, all_channel_id, change_del_channel
from bot_app.misc import dp, bot


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='delete-from-channel')
async def del_channel_menu(call: CallbackQuery):
    await call.answer()
    chat_id = call.data.split('_')[1]
    data_channels = await bot_app.db.chat_settings.get_target_channels(int(chat_id))
    dict_inline_markup = dict()
    for i_channel in data_channels:
        channel_id = i_channel['channel_id']
        channel_info = await bot.get_chat(channel_id)
        dict_inline_markup[channel_id] = [channel_info['title'], chat_id]
    await call.message.edit_text(text='Выберите канал для удаления:',
                                 reply_markup=all_channel_id(chat_id, dict_inline_markup))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='delete-channel')
async def del_channel_question(call: CallbackQuery):
    await call.answer()
    chat_id = call.data.split('_')[1]
    channel_id = call.data.split('_')[2]
    await call.message.edit_text('Вы действительно хотите удалить канал?',
                                 reply_markup=change_del_channel(chat_id, channel_id))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='sure-delete-channel')
async def del_channel(call: CallbackQuery):
    await call.answer()
    chat_id = call.data.split('_')[1]
    channel_id = call.data.split('_')[2]
    await bot_app.db.chat_settings.delete_target_channel(int(chat_id), int(channel_id))
    chat_settings = await get_chat_settings(int(chat_id))
    await call.message.edit_text(text='Канал успешно удален',
                                 reply_markup=settings_button(chat_id, chat_settings))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='cancellation_')
async def cancel_del_channel(call: CallbackQuery):
    await call.answer()
    chat_id = call.data.split('_')[1]
    chat_settings = await get_chat_settings(int(chat_id))
    await call.message.edit_text(text='Удаление канала отменено',
                                 reply_markup=settings_button(chat_id, chat_settings))

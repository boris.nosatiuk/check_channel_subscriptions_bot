import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import CallbackQuery, Message
from aiogram.utils.exceptions import ChatNotFound

import bot_app.db.chat_settings
from bot_app import config
from bot_app.db.chat_settings import get_chat_settings
from bot_app.keyboard.inline_markup import cancellation, settings_button
from bot_app.keyboard.reply_echo_admin_button import admin_cancel_button, admin_echo_button
from bot_app.misc import dp, bot


class NewChannel(StatesGroup):
    new_channel = State()


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                           text_startswith='add-to-channel')
async def settings_menu(call: CallbackQuery, state: FSMContext):
    await call.answer()
    chat_id = call.data.split('_')[1]
    await call.message.edit_reply_markup(None)
    # await call.message.delete_reply_markup()
    await call.message.edit_text(text='Введите id канала:', reply_markup=cancellation(chat_id))

    await NewChannel.new_channel.set()
    await state.set_data({'chat_id': chat_id})


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='cancellation',
                           state=NewChannel.new_channel)
async def cancel_new_channel(call: CallbackQuery, state: FSMContext):
    await call.answer()
    chat_id = call.data.split('_')[1]
    await state.finish()
    chat_settings = await get_chat_settings(int(chat_id))
    await call.message.edit_text('Добавление канала отменено', reply_markup=settings_button(chat_id, chat_settings))


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), state=NewChannel.new_channel)
async def get_new_channel(message: Message, state: FSMContext):
    chat_id = await state.get_data()
    data = await state.get_data()
    chat_settings = await get_chat_settings(data['chat_id'])
    try:
        new_channel = int(message.text)
    except ValueError:
        await bot.send_message(message.from_user.id,
                               text='Ошибка ввода. Введите id_канала в формате "-10000000000000", или нажмите "Отмена"',
                               reply_markup=cancellation(chat_id['chat_id']))
        return
    try:
        await bot.get_chat_administrators(new_channel)
        new_channel_info = await bot.get_chat(new_channel)
        await bot_app.db.chat_settings.add_target_channel(chat_id['chat_id'], new_channel_info['id'],
                                                          new_channel_info['invite_link'])
        await state.finish()

        await bot.send_message(message.from_user.id, text='Канал успешно добавлен',
                               reply_markup=settings_button(chat_id['chat_id'], chat_settings))
    except ChatNotFound:
        await state.finish()
        await bot.send_message(message.from_user.id, text='Действие невозможно. Добавьте бот в администраторы канала',
                               reply_markup=settings_button(chat_id['chat_id'], chat_settings))


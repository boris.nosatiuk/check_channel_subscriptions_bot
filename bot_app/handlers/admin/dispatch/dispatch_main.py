import aiogram
from bot_app import config
import bot_app.db.users
from aiogram import types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import Dispatcher, FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.utils import executor
from bot_app.misc import dp, bot

from bot_app.keyboard.reply_echo_admin_button import admin_echo_button, admin_cancel_button, admin_choice_button


class NewNotificationText(StatesGroup):
    new_notifier_text = State()


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text='Создать рассылку')
async def mass_send(message: types.Message):
    await bot.send_message(message.from_user.id, text='Введите сообщение которое будет разослано:',
                           reply_markup=admin_cancel_button())
    await NewNotificationText.new_notifier_text.set()


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                    text='Разослать', state=NewNotificationText.new_notifier_text)
async def mass_send(message: types.Message, state: FSMContext):
    users_dict = await bot_app.db.users.get_all()
    data = await state.get_data()

    chat_id = data['chat_id']
    message_id = data['message_id']

    await state.finish()
    errors_log_dict = dict()
    counter_successful_shipment = 0
    text_error = 'Тип ошибки   -   количество\n'
    for i_user in users_dict:
        try:
            await bot.copy_message(i_user['telegram_id'], from_chat_id=chat_id, message_id=message_id)
            counter_successful_shipment += 1
        except BaseException as err:
            if err in errors_log_dict.keys():
                errors_log_dict[err] += 1
            else:
                errors_log_dict[err] = 1
    for key_error, value_error in errors_log_dict.items():
        text_error += f'{key_error} - {value_error}\n'
    counter_unsuccessful_shipment = sum(errors_log_dict.values())

    result_mailing = f'Рассылка сообщений завершена.\n' \
                     f'Всего разослано: {counter_successful_shipment+counter_unsuccessful_shipment} сообщений\n' \
                     f'Удачных доставок:  {counter_successful_shipment}\n' \
                     f'Неудачных доставок: {counter_unsuccessful_shipment}\n' \
                     f'Список ошибок по неудачным доставкам:\n' \
                     f'{text_error}'
    await bot.send_message(message.from_user.id, text=result_mailing, reply_markup=admin_echo_button())


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID),
                    text='Отмена', state=NewNotificationText.new_notifier_text)
async def mass_send(message: types.Message, state: FSMContext):
    await state.finish()
    await bot.send_message(message.from_user.id, text='Отправка сообщения отменена!',
                           reply_markup=admin_echo_button())


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), content_types=aiogram.types.ContentType.ANY,
                    state=NewNotificationText.new_notifier_text)
async def mass_send(message: types.Message, state: FSMContext):
    await state.set_data({'message_id': message.message_id, 'chat_id': message.chat.id})

    await NewNotificationText.new_notifier_text.set()
    await bot.send_message(message.from_user.id, text='Сообщение готово к отправке. '
                                                      'Выберите действие', reply_markup=admin_choice_button())




import aiogram.types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters import state
from aiogram.types import CallbackQuery, Message
from aiogram.utils.exceptions import ChatNotFound

from bot_app import config
from bot_app.db.chat_settings import get_all_chat_id, get_chat_settings
from bot_app.keyboard.inline_markup import settings_button
from bot_app.keyboard.inline_markup import all_chat_id
from bot_app.misc import dp, bot, redis
from bot_app.keyboard.reply_echo_admin_button import admin_echo_button, admin_cancel_button


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), commands=['admin', 'start'], state='*')
async def admin_echo(message: Message, state: FSMContext):
    await state.finish()
    await bot.send_message(chat_id=message.from_user.id, text=f'Приветствую {message.from_user.username}',
                           reply_markup=admin_echo_button())


@dp.message_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text='Настройка чатов')
async def choice_channel(message: Message):
    all_chat_dict = await get_all_chat_id()
    chat_data_list = []
    for chat_id in all_chat_dict:
        try:
            chat_data = await bot.get_chat(chat_id['chat_id'])
            chat_data_list.append(chat_data)
        except ChatNotFound:
            await bot.send_message(chat_id=message.from_user.id,
                                   text=f'Бот не админ в одном из чатов.')
    await bot.send_message(chat_id=message.from_user.id, text='Выберите чат для настройки:',
                           reply_markup=all_chat_id(chat_data_list))


@dp.callback_query_handler(aiogram.filters.IDFilter(user_id=config.ADMINS_ID), text_startswith='edit-channel')
async def settings_menu(call: CallbackQuery):
    chat_id = call.data.split('_')[1]
    chat_settings = await get_chat_settings(int(chat_id))
    await call.answer()
    await call.message.edit_text(text='Меню настроек чата:',
                                 reply_markup=settings_button(chat_id, chat_settings))


@dp.message_handler(aiogram.filters.ChatTypeFilter(aiogram.types.ChatType.PRIVATE))
async def eny_text_messages(message: Message):
    if message.from_user.id not in config.ADMINS_ID:
        await bot.send_message(chat_id=message.from_user.id, text='Вы не являетесь админом данного бота!')
        return

    await bot.send_message(chat_id=message.from_user.id,
                           text='Ошибочный ввод. Возможно вы имели ввиду: /start или /admin')

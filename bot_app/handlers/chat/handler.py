import json
import datetime
from zoneinfo import ZoneInfo

import aiogram.types
from aiogram.types import ChatMemberUpdated
from aiogram.utils.exceptions import BadRequest

import bot_app.db.users
from bot_app.keyboard.inline_markup import url_inline
from bot_app.misc import dp, bot, redis
from bot_app.misc import scheduler


async def ban_user(chat_id, user_id):
    await bot.ban_chat_member(chat_id=chat_id, user_id=user_id)


async def kick_user(chat_id, user_id):
    await bot.kick_chat_member(chat_id=chat_id, user_id=user_id)


async def delete_message(chat_id, message_id):
    await bot.delete_message(chat_id=chat_id, message_id=message_id)


@dp.chat_member_handler()
async def chat_member_update(event: ChatMemberUpdated):
    if event.chat.type == aiogram.types.ChatType.CHANNEL:
        return
    target_channel_id = await bot_app.db.chat_settings.get_target_channels(event.chat.id)
    chat_settings = await bot_app.db.chat_settings.get_chat_settings(event.chat.id)
    if chat_settings['bot_enabled'] != 1:
        return
    if chat_settings['skipVerifiedUsers'] == 1:
        chat_dict = await bot_app.db.chat_users.get_users_chats(event.new_chat_member.user.id)
        if chat_dict is not None:
            return
    await bot_app.db.chat_settings.create_chat(event.chat.id)
    if event.new_chat_member.status != 'member':
        return
    if event.old_chat_member.status == event.new_chat_member.status:
        return

    is_member = True
    for channel in target_channel_id:
        chat_member = await bot.get_chat_member(chat_id=channel['channel_id'],
                                                user_id=event.new_chat_member.user.id)
        if chat_member.status not in ['owner', 'member', 'creator', 'admin']:
            is_member = False
            break

    if not is_member:
        await bot.restrict_chat_member(chat_id=event.chat.id,
                                       user_id=event.new_chat_member.user.id,
                                       can_send_messages=False)
        try:
            message_to_new_user = await bot.send_photo(chat_id=event.chat.id,
                                                       photo=chat_settings['greetings_message_photo'],
                                                       caption=chat_settings['greetings_message_text'],
                                                       reply_markup=url_inline(target_channel_id))
        except BadRequest:
            message_to_new_user = await bot.send_message(chat_id=event.chat.id,
                                                         text=chat_settings['greetings_message_text'],
                                                         reply_markup=url_inline(target_channel_id))
        del_time_mess = datetime.datetime.now(tz=ZoneInfo("Europe/Minsk")) + datetime.timedelta(
            seconds=chat_settings['time_to_pass_captcha'])
        scheduler.add_job(delete_message, 'date', run_date=del_time_mess,
                          args=(event.chat.id, message_to_new_user.message_id),
                          id=f'del_message_id_{message_to_new_user.message_id}_{event.new_chat_member.user.id}')

        ban_kick_time_user = datetime.datetime.now(tz=ZoneInfo("Europe/Minsk")) + datetime.timedelta(
            seconds=chat_settings['time_to_pass_captcha'])

        if chat_settings['ban_from_chat_on_fail'] == 1:
            scheduler.add_job(ban_user, 'date', run_date=ban_kick_time_user,
                              args=(event.chat.id, event.new_chat_member.user.id),
                              id=f'ban_{event.chat.id}_{event.new_chat_member.user.id}',
                              name=f'user_{event.new_chat_member.user.id}')

        elif chat_settings['kick_from_chat_on_fail'] == 1:
            scheduler.add_job(kick_user, 'date', run_date=ban_kick_time_user,
                              args=(event.chat.id, event.new_chat_member.user.id),
                              id=f'kick_{event.chat.id}_{event.new_chat_member.user.id}',
                              name=f'user_{event.new_chat_member.user.id}')
        else:
            await bot.restrict_chat_member(chat_id=event.chat.id, user_id=event.from_user.id,
                                           can_send_messages=True)
            return

        await redis.set(f'user-verification_{event.from_user.id}_{event.chat.id}', json.dumps(
            {'message_id': message_to_new_user.message_id, 'chat_id': event.chat.id}))

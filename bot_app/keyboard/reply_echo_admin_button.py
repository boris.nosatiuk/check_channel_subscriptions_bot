from aiogram.types import KeyboardButton, ReplyKeyboardMarkup


def admin_echo_button():
    full_kb = ReplyKeyboardMarkup(resize_keyboard=True)
    button1 = KeyboardButton('Создать рассылку')
    button2 = KeyboardButton('Настройка чатов')
    full_kb.row(button1, button2)
    return full_kb


def admin_cancel_button():
    full_kb = ReplyKeyboardMarkup(resize_keyboard=True)
    button1 = KeyboardButton('Отмена')
    full_kb.row(button1)
    return full_kb


def admin_choice_button():
    full_kb = ReplyKeyboardMarkup(resize_keyboard=True)
    button1 = KeyboardButton('Разослать')
    button2 = KeyboardButton('Отмена')
    full_kb.row(button1, button2)
    return full_kb
